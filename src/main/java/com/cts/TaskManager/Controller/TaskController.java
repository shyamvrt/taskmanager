package com.cts.TaskManager.Controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.cts.TaskManager.Model.*;
import com.cts.TaskManager.Service.TaskService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class TaskController {
	@Autowired
	TaskService taskService;

	// View all tasks
	@GetMapping("/viewTask")
	public List<Task> getAllTasks() {

		return taskService.viewAllTask();

	}
	//view a task
	
	

	

	@PutMapping("/task/{id}")
	public void updateTopic(@RequestBody Task task, @PathVariable long id) {
		taskService.updateTask( task);
	}

	@PostMapping("/addTask")
	public void addTask(@RequestBody Task task) {
	
	
		taskService.addTask(task);

	}
}
