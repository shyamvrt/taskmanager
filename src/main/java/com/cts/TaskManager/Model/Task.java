package com.cts.TaskManager.Model;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class Task {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "task_Id")
	private long taskId;

	//@OneToOne(fetch = FetchType.LAZY, mappedBy = "task", cascade = CascadeType.ALL)
	//@JoinColumn(name="parentId",referencedColumnName="id")
	  
	// @JoinColumn(name="id")
	

	@Column(name = "task")
	private String task;
	
	@Column(name="parentTask")
	private String parentTask;
	
	@Column(name = "start_Date")
	private LocalDate startDate;

	@Column(name = "end_Date")
	private LocalDate endDate;

	@Column(name = "priority")
	private int priority;

	public Task(  String task, String parentTask,LocalDate startDate, LocalDate endDate, int priority) {
		super();
		this.task = task;
		this.parentTask = parentTask;
		
		this.startDate = startDate;
		this.endDate = endDate;
		this.priority = priority;
	}

	public long getTaskId() {
		return taskId;
	}

	public void setTaskId(long taskId) {
		this.taskId = taskId;
	}

	public String getParentTask() {
		return parentTask;
	}

	public void setParentTask(String parentTask) {
		this.parentTask = parentTask;
	}

	public String getTask() {
		return task;
	}

	public void setTask(String task) {
		this.task = task;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public Task() {
		super();
	}

}
