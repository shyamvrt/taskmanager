package com.cts.TaskManager.Repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.cts.TaskManager.Model.Task;
@Repository
public interface TaskRepository extends CrudRepository<Task,Long>{

}
