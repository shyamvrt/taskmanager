package com.cts.TaskManager.Service;


import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cts.TaskManager.Model.Task;
import com.cts.TaskManager.Repo.TaskRepository;



@Service
public class TaskService {
	@Autowired
	TaskRepository repo;
	
//	Task dummyTask1=new Task(100,"testing task 1",LocalDate.of(2018, 12, 13),LocalDate.of(2019, 01, 25),3); 
//	Task dummyTask2=new Task(101,"testing task 2",LocalDate.of(2018, 02, 11),LocalDate.of(2019, 02, 26),2); 
//	ArrayList <Task> taskList=new ArrayList<>();	
	
	
	
//	public List<Task> addingADummyTask(){
//		taskList.add(dummyTask1);
//		taskList.add(dummyTask2);
//		return taskList;
//		
//	}
	
	
	public List<Task> viewAllTask(){
		
		//ArrayList <Task> taskList1=new ArrayList<>();
		
		//taskList1.add(dummyTask1);
		//taskList1.add(dummyTask2);
		//addTask(taskList1);
		//repo.findAll().forEach(taskList::add);
		//taskList.addAll(addADummyTask());
		
		return (List<Task>) repo.findAll();
		
	}
	

	
	public void addTask(Task task){
		repo.save(task);
		
		
	}
	

	public void updateTask(Task task){
	
		repo.save(task);
		
		
	}


	
	
	
	
}
